import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { environment } from '../../environments/environment';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
      private readonly _http: HttpClient
  ) { }

    /**
     * returns list of all users
     * @returns {Observable<User>}
     */
  public getUsers(): Observable<User[]> {
    return this._http.get<User[]>(`${environment.apiUrl}`);
  }
}
