import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { User } from '../../interfaces/user';
import { Subscription } from 'rxjs/index';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit, OnDestroy {
  public users: User[] = [];
  public subscriptions: Subscription[] = [];

  constructor(
      private readonly _apiService: ApiService,
  ) { }

  ngOnInit() {
    this.getAllUsers();
  }

    /**
     * retrieves the list of all users
     * @returns void
     */
  public getAllUsers(): void {
    this._apiService.getUsers()
      .subscribe((usersList) => {
        this.users = usersList;
      }, (error) => {
        console.log(error);
      });
  }

    /**
     * removes specified user from the general list
     * @param {number} userId - user id to be removed from the general list
     */
  public removeUser(userId: number): void {
    this.users = this.users.filter((user) => user.id !== userId);
  }

    /**
     * checks if user has reached 18 years of age
     * @param age{number} - age of the user
     * @returns {string}
     */
  public passingAgeLimit(age: number): string {
    return (age > 18) ? 'green' : 'red';
  }

  public ngOnDestroy(): void {
    this.subscriptions.forEach((s) => s.unsubscribe());
  }

}
